<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;

use App\Services\RegisterService;

class RegisterController extends Controller
{
    public function register(RegisterUserRequest $request, RegisterService $service)
    {
        DB::transaction(fn () => $service->createAddress($request->validated())
                                ->registerPatientIfNotExist()
                                ->registerUserAndAttachToPatient()
                                ->loginUserAfterRegister());

        return response()->json([
            'success' => 'Usuario registrado con éxito'
        ]);
    }
}
