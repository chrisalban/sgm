<?php

namespace App\Http\Controllers;

use App\Http\Repositories\ExamRepository;
use App\Http\Requests\StoreExamRequest;
use App\Http\Requests\UpdateExamRequest;
use App\Http\Requests\UploadFileExamRequest;
use App\Http\Resources\ExamResource;
use App\Models\Exam;
use App\Models\File;
use App\Services\ExamService;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    public function __construct(
        protected ExamService $service,
        protected ExamRepository $exam
    ) {
        $this->middleware('auth:sanctum');
        $this->authorizeResource(Exam::class, 'exam');
    }

    public function index(Request $request)
    {
        return ExamResource::collection($this->exam->getPendingExams($request->filter_by)); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExamRequest $request)
    {
        $this->service->createExam($request->validated(), $request->appointment);
        return response()->json([
           'success' => 'exam created'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateExamRequest $request, Exam $exam)
    {
        $this->service->updateExam($request->validated(), $exam);
        return response()->json([
            'success' => 'exam updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        $this->service->deleteExam($exam);
        return response()->json([
            'success' => 'exam deleted'
        ]);
    }

    public function uploadFile(UploadFileExamRequest $request, Exam $exam)
    {
        $this->authorize('create', Exam::class);
        $this->service->uploadFile($exam, $request->file);

        return response()->json([
            'success' => 'file uploaded'
        ]);
    }

    public function deleteFile(Exam $exam, File $file)
    {
        $this->authorize('create', Exam::class);
        $this->service->deleteFile($file);

        return response()->json([
            'success' => 'file deleted'
        ]);
    }

    public function process(Exam $exam)
    {
        $this->authorize('create', Exam::class);
        $this->service->process($exam);

        return response()->json([
            'success' => 'exam processed'
        ]);
    }

    public function getReport(Request $request)
    {
        $this->authorize('viewAny', Exam::class);
        return ExamResource::collection($this->exam->getAllExams($request->filter_by));
    }
}
