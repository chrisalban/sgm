<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;
use App\Http\Resources\PatientListResource;
use App\Http\Resources\PatientShowResource;
use App\Http\Requests\StorePatientRequest;
use App\Http\Requests\UpdatePatientRequest;
use App\Http\Repositories\PatientRepository;
use App\Services\PatientService;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{ 
    public function __construct(
        protected PatientRepository $patient,
        protected PatientService $service
    ) {
        $this->middleware('auth:sanctum');
        $this->authorizeResource(Patient::class, 'patient');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return PatientListResource::collection($this->patient->getPatientsFromCurrentUser($request->filter_by)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePatientRequest $request)
    {
        DB::transaction(fn() => $this->service
                                    ->createAddress($request->address))
                                    ->createPatient($request->validated());

        return response()->json([ 'success' => 'patient created' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        return new PatientShowResource($patient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePatientRequest $request, Patient $patient)
    {
        DB::transaction(fn() => $this->service
                                    ->updateAddress($request->address, $patient->address))
                                    ->updatePatient($request->validated(), $patient);

        return response()->json([ 'success' => 'patient updated' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        $this->service->deletePatientIfDoesntHaveAppointments($patient);
        return response()->json([ 'success' => 'patient deleted' ]);
    }
}
