<?php

namespace App\Http\Repositories;

use Illuminate\Support\Facades\Auth;

use App\Models\Appointment;

/**
 * Repositorio para la clase de citas
 */
class AppointmentRepository
{
    public function appointmentsFromCurrentUser(String $filter_by = null, String $time = null)
    {
        $appointments = Appointment::filterByPatientName($filter_by)
                                   ->with('patient', 'patient.users', 'doctor');

        if ($time) {
            $appointments->orderBy('start_time', 'ASC')
                         ->whereDate('start_time', $time);
        } else {
            $appointments->orderBy('start_time', 'DESC');
        }

        $user = Auth::user();
        
        // Si el usuario no es administrador consultar solo los que le pertenecen
        if (!$user->is_admin) {
            if ($user->is_client) {
                // Consultar las citas de un cliente
                $ids = $user->patients->pluck('id')->all();
                $appointments->whereHas('patient', fn ($patient) => $patient->whereIn('id', $ids));
            } else {
                // Consultar las citas de un doctor
                $appointments->whereHas('doctor', fn ($doctor) => $doctor->where('id', $user->id));
            }
        }

        return $appointments;
    }

    public function appointmentsFromDoctor(int $doctor_id)
    {
        return Appointment::whereDate('start_time', '>=', now())
                          ->where('is_cancelled', false)
                          ->whereHas('doctor', fn ($doctor) => $doctor->where('id', $doctor_id))
                          ->orderBy('start_time');
    }

    public function showAppointment(Appointment $appointment)
    {
        $appointment->load([ 'patient', 'doctor', 'speciality', 'prescriptions', 'exams.files' ]);
        return $appointment;
    }
}
