<?php
namespace App\Http\Repositories;

use App\Models\Prescription;
use Illuminate\Support\Facades\Auth;

/*
 * Repositorio para las recetas
 */
class PrescriptionRepository
{
    /*
     * Método que devuelve las recetas de un cliente de la mas reciente a la mas antigua
     */
    public function getPrescriptions(string $filter_by = null)
    {
        $prescriptions = Prescription::with('appointment.doctor', 'appointment.patient')
                                     ->filterPrescriptionByNameOrDoctorOrPatient($filter_by);

        $user = Auth::user();
        
        // Si el usuario no es administrador consultar solo los que le pertenecen
        if (!$user->is_admin) {
            if ($user->is_client) {
                // Consultar las recetas de un cliente
                $ids = $user->patients->pluck('id')->all();
                $prescriptions->whereHas('appointment.patient', fn ($patient) => $patient->whereIn('id', $ids));
            } else {
                // Consultar las recetas de un doctor
                $prescriptions->whereHas('appointment.doctor', fn ($doctor) => $doctor->where('id', $user->id));
            }
        }

        return $prescriptions->latest()->get();
    }
}
