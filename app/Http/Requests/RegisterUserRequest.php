<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'identification_card'   => ['required', 'string', 'max:10', 'regex:/^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$/', 'unique:users,identification_card'],
            'name'                  => 'required|string',
            'last_name'             => 'required|string',
            'username'              => 'required|string|unique:users,username',
            'email'                 => 'required|email|unique:users,email',
            'password'              => 'required|min:8|confirmed',
            'phone'                 => ['required', 'string', 'regex:/^(09|0[2-7])([0-9]{8}|[0-9]{6})$/'],
            'birthdate'             => 'required|date',
            'gender'                => 'required|string|in:F,M',
            'main_street'           => 'required|string',
            'secondary_street'      => 'required|string',
            'province'              => 'required|string',
            'city'                  => 'required|string'
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'province'  => Str::upper($this->province),
            'city'      => Str::upper($this->city)
        ]);
    }
}
