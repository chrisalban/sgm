<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $start = now();
        $end = now()->add(30, 'minutes');

        return [
            'start_time'    => "required|date_format:Y-m-d H:i:s|after:$start",
            'end_time'      => "required|date_format:Y-m-d H:i:s|after:$end",
            'doctor'        => 'required|integer|exists:users,id',
            'patient'       => 'required|integer|exists:patients,id',
            'speciality'    => 'required|integer|exists:specialities,id'
        ];
    }
}
