<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

use App\Models\Patient;

class StorePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'identification_card'       => ['required', 'string', 'max:10', 'regex:/^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$/', 'unique:patients,identification_card'],
            'name'                      => 'required|string',
            'last_name'                 => 'required|string',
            'email'                     => 'required|email|unique:patients,email',
            'phone'                     => ['required', 'string', 'regex:/^(09|0[2-7])([0-9]{8}|[0-9]{6})$/'],
            'birthdate'                 => 'required|date',
            'gender'                    => 'required|string|in:F,M',
            'address.main_street'       => 'required|string',
            'address.secondary_street'  => 'required|string',
            'address.province'          => 'required|string',
            'address.city'              => 'required|string',
            'relationship'              => 'sometimes|required|in:' . Patient::SPOUSE_RELATIONSHIP . ',' . Patient::SON_RELATIONSHIP . ',' . Patient::DAUGHTER_RELATIONSHIP . ',' . Patient::OTHERS_RELATIONSHIP
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'province'  => Str::upper($this->province),
            'city'      => Str::upper($this->city)
        ]);
    }
}
