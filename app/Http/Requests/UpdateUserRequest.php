<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'identification_card'   => ['required', 'string', 'max:10', 'regex:/^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$/', "unique:users,identification_card,$this->id"],
            'name'                  => 'required|string',
            'last_name'             => 'required|string',
            'username'              => "required|string|unique:users,username,$this->id",
            'email'                 => "required|email|unique:users,email,$this->id",
            'phone'                 => ['required', 'string', 'regex:/^(09|0[2-7])([0-9]{8}|[0-9]{6})$/'],
            'birthdate'             => 'required|date',
            'gender'                => 'required|string|in:F,M',
            'password'              => 'sometimes|required|min:8|confirmed',
            'role'                  => 'required|exists:roles,name',
            'address.main_street'       => 'required|string',
            'address.secondary_street'  => 'required|string',
            'address.province'          => 'required|string',
            'address.city'              => 'required|string',
            'specialities'          => 'nullable|array',
            'specialities.*.id'     => 'required|exists:specialities,id'
        ];
    }
}
