<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'start_time'    => $this->start_time,
            'end_time'      => $this->end_time,
            'description'   => $this->description,
            'closed_at'     => $this->closed_at,
            'is_cancelled'  => $this->is_cancelled,
            'patient'       => PatientShowResource::make($this->whenLoaded('patient')),
            'doctor'        => UserShowResource::make($this->whenLoaded('doctor')),
            'speciality'    => SpecialityResource::make($this->whenLoaded('speciality')),
            'prescriptions' => PrescriptionResource::collection($this->whenLoaded('prescriptions')),
            'exams'         => ExamResource::collection($this->whenLoaded('exams'))
        ];
    }
}
