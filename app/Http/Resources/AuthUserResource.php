<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'identification_card'   => $this->identification_card,
            'name'                  => $this->name,
            'last_name'             => $this->last_name,
            'username'              => $this->username,
            'email'                 => $this->email,
            'role'                  => [
                'name' => $this->role->name
            ]
        ];
    }
}
