<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PatientShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = Auth::user();
        return [
            'id'                    => $this->id,
            'name'                  => $this->name,
            'last_name'             => $this->last_name,
            'phone'                 => $this->phone,
            'identification_card'   => $this->identification_card,
            'email'                 => $this->email,
            'birthdate'             => $this->birthdate,
            'gender'                => $this->gender,
            'relationship'          => $this->when($user->is_client, fn () => $this->users()->firstWhere('id', $user->id)->relationship),
            'address'               => [
                'id'                => $this->address->id,
                'main_street'       => $this->main_street,
                'secondary_street'  => $this->secondary_street,
                'province'          => $this->province,
                'city'              => $this->city,
            ]
        ];
    }
}
