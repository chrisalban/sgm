<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'last_name'     => $this->last_name,
            'specialities'  => SpecialityResource::collection($this->whenLoaded('specialities')),
            'role'          => RoleResource::make($this->whenLoaded('role'))
        ];
    }
}
