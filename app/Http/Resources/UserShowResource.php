<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'name'                  => $this->name,
            'last_name'             => $this->last_name,
            'identification_card'   => $this->identification_card,
            'username'              => $this->username,
            'email'                 => $this->email,
            'phone'                 => $this->phone,
            'birthdate'             => $this->birthdate,
            'gender'                => $this->gender,
            'role'                  => RoleResource::make($this->whenLoaded('role')),
            'specialities'          => SpecialityResource::collection($this->whenLoaded('specialities')),
            'address'               => [
                'id'                => $this->address->id,
                'main_street'       => $this->address->main_street,
                'secondary_street'  => $this->address->secondary_street,
                'province'          => $this->address->province,
                'city'              => $this->address->city,
            ]
        ];
    }
}
