<?php

namespace App\Listeners;

use App\Notifications\AppointmentCancelled as NotificationsAppointmentCancelled;
use App\Events\AppointmentCancelled;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class AppointmentCancelledNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AppointmentCancelled  $event
     * @return void
     */
    public function handle(AppointmentCancelled $event)
    {
        Notification::send([
            $event->appointment->doctor,
            $event->appointment->patient
        ], new NotificationsAppointmentCancelled($event->appointment));
    }
}
