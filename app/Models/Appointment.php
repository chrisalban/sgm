<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Filterable;

class Appointment extends Model
{
    use HasFactory, Filterable;

    protected $fillable = [
        'start_time', 'end_time', 'description', 'is_cancelled', 'closed_at'
    ];

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function doctor()
    {
        return $this->belongsTo(User::class, 'doctor_id');
    }

    public function speciality()
    {
        return $this->belongsTo(Speciality::class);
    }

    public function prescriptions()
    {
        return $this->hasMany(Prescription::class);
    }

    public function exams()
    {
        return $this->hasMany(Exam::class);
    }

    public function previous()
    {
        return $this->belongsTo(Appointment::class, 'previous_id');
    }

    public function next()
    {
        return $this->hasOne(Appointment::class, 'previous_id');
    }
}
