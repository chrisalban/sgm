<?php
namespace App\Services;

use App\Models\Exam;
use App\Models\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/*
 * Servicio para los examenes
 */
class ExamService
{
    public function __construct(
        protected Exam $exam
    ) {}

    public function createExam(array $fields, int $appointment)
    {
        $this->exam = new Exam($fields);
        $this->exam->appointment()->associate($appointment)->save();

        return $this;
    }

    public function updateExam(array $fields, Exam $exam)
    {
        $this->exam = $exam;
        $this->exam->update($fields);

        return $this;
    }

    public function deleteExam(Exam $exam)
    {
        $this->exam = $exam;
        $this->exam->delete();

        return $this;
    }

    public function uploadFile(Exam $exam, UploadedFile $file)
    {
        $path = $file->store($exam->id, 'public');
        
        $file = new File([
            'url'  => $path,
            'name' => $file->getClientOriginalName()
        ]);
        $file->technician()->associate(Auth::user());

        $exam->files()->save($file);

        return $this;
    }

    public function deleteFile(File $file)
    {
        Storage::disk('public')->delete($file->url);
        $file->delete();

        return $this;
    }

    public function process(Exam $exam)
    {
        $exam->update([ 'status' => Exam::READY_STATUS ]);

        return $this;
    }
}
