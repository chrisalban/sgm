<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;

use App\Models\Patient;
use App\Models\Address;

/**
 * Servicio para los pacientes
 */
class PatientService
{
    public function __construct(
        protected Patient $patient,
        protected Address $address
    ) {

    }

    /*
    * Metodo que crea un paciente
    */
    public function createPatient(array $fields)
    {
        $this->patient = Patient::make($fields);
        $this->patient->address()->associate($this->address)->save();

        if (isset($fields['relationship'])) {
            $this->attachUserIfClient($fields['relationship']);
        }

        return $this;
    }

    /*
    * Metodo que actualiza un paciente
    */
    public function updatePatient(array $fields, Patient $patient)
    {
        $patient->update($fields);

        $this->patient = $patient;
        if (isset($fields['relationship'])) {
            $this->updateUserIfClient($fields['relationship']);
        }

        return $this;
    }

    /*
    * Metodo que guarda la dirección al paciente creado
    */
    public function createAddress(array $address)
    {
        $this->address = Address::create($address);
        return $this;
    }

    /*
    * Metodo que actualiza la dirección del paciente
    */
    public function updateAddress(array $fields, Address $address)
    {
        $address->update($fields);
        return $this;
    }

    /*
    * Metodo que ata el usuario al paciente si se trata de un cliente y la relación con el paciente
    */
    private function attachUserIfClient(String $relationship)
    {
        $user = Auth::user();
        if ($user->is_client) {
            $this->patient->users()->attach($user, [
                'relationship' => $relationship
            ]);
        }
    }

    /*
    * Metodo que actualiza el usuario al paciente si se trata de un cliente y la relación con el paciente
    */
    private function updateUserIfClient(String $relationship)
    {
        $user = Auth::user();
        if ($user->is_client) {
            $this->patient->users()->updateExistingPivot($user, [
                'relationship' => $relationship
            ]);
        }
    }

    /*
    * Metodo que elimina un paciente de no tener datos atados o si es de un cliente elimina la relación
    */
    public function deletePatientIfDoesntHaveAppointments(Patient $patient)
    {
        $user = Auth::user();

        if ($user->hasAnyRole(['admin', 'super'])) {
            $this->validatePatientDoesntHaveAppointments($patient);
            $patient->delete();
        } else if ($user->is_client) {
            $relationship = $patient->users()->firstWhere('id', $user->id)->relationship;
            if($relationship !== Patient::HEADLINE_RELATIONSHIP) $patient->users()->detach($user);
            else abort(422, 'patient can not delete');
        }

        return true;
    }

    private function validatePatientDoesntHaveAppointments(Patient $patient)
    {
        if ($patient->appointments()->count()) return abort(422, 'patient has appointments');
    }
}
