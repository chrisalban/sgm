<?php
namespace App\Services;

use App\Models\Prescription;

/*
* Servicio para la clase de recetas
*/
class PrescriptionService
{
    public function __construct(
       protected Prescription $prescription 
    ) {}

    /*
    * Método que crea una receta y la ata a una cita médica
    */
    public function storePrescription(array $fields, int $appointment)
    {
        $this->prescription = new Prescription($fields);
        $this->prescription->appointment()->associate($appointment)->save();

        return $this;
    }

    /*
    * Método que actualiza una receta médica
    */
    public function updatePrescription(array $fields, Prescription $prescription)
    {
        $this->prescription = $prescription;
        $this->prescription->update($fields);

        return $this;
    }

    /*
     * Método que elimina la receta
     */
    public function deletePrescription(Prescription $prescription)
    {
        $this->prescription = $prescription;
        $this->prescription->delete();

        return $this;
    } 
}
