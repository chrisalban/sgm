<?php
namespace App\Services;

use App\Models\Address;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

/**
 * Servicio para los usuarios
 */
class UserService
{
    public function __construct(
        public User $user,
        protected Address $address
    ) {}


    /*
     * Método que crea una dirección
     */
    public function createAddress(array $fields)
    {
        $this->address = Address::create($fields);

        return $this;
    }

    /*
    *
    */
    public function updateAddress(array $fields, Address $address)
    {
        $this->address = $address;
        $this->address->update($fields);

        return $this;
    }

    /*
    * Metodo que hace un usuario pero no lo guarda todabía en la base de datos
    */
    public function makeUser(array $fields)
    {
        $fields = collect($fields);

        $this->user = User::make($fields->except([ 'password' ])->all());

        if ($fields->has('password')) {
            $this->user->forceFill([
                'password' => Hash::make($fields->get( 'password' ))
            ]);
        }

        $this->user->address()->associate($this->address);

        $this->user->email_verified_at = now();

        return $this;
    }

    /*
    * Metodo que actualiza un usuario
    */
    public function updateUser(array $fields, User $user)
    {
        $fields = collect($fields);

        $user->update($fields->except([ 'password' ])->all());

        if ($fields->has('password')) {
            $user->forceFill([
                'password' => Hash::make($fields->get( 'password' ))
            ]);
        }

        $this->user = $user;

        return $this;
    }

    /*
    * Metodo que asigna un rol a un usuario
    */
    public function assignRole(array $role)
    {
        $this->user->assignRole($role['name'])->save();
        return $this;
    }

    /*
    * Metodo que asocia las especialidades al usuario
    */
    public function attachSpecialities(array $specialities)
    {
        $this->validateSpecialitiesOnDoctorRole($specialities);

        $array = $this->getSpecialities($specialities);
        $this->user->specialities()->attach($array);

        return true;
    }

    /*
    * Metodo que sincroniza las especialidades del usuario
    * Si es un administrador se eliminan todas las especialidades
    */
    public function syncSpecialities(array $specialities)
    {
        $this->validateSpecialitiesOnDoctorRole($specialities);

        if ($this->user->is_doctor)
        {
            $array = $this->getSpecialities($specialities);
            $this->user->specialities()->sync($array);

        } else $this->user->specialities()->detach();

        return true;
    }

    public function restoreUser(String $identification_card)
    {
        return User::withTrashed()->firstWhere('identification_card', $identification_card)->restore();
    }

    /*
    * Metodo que extrae los ids de las especialidades en un solo array
    */
    protected function getSpecialities(array $specialities) {
        return collect($specialities)->reduce(function($carry, $specility) {
            $carry[] = $specility['id'];
            return $carry;
        }, []);
    }

    /*
    * Metodo que valida que si el rol es de un doctor se asigne al menos una especialidad
    */
    protected function validateSpecialitiesOnDoctorRole(array $specialities) {
        if($this->user->is_doctor && !count($specialities)) return abort(422, 'empty specialities');
    }
}
