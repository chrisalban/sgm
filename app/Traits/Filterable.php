<?php
namespace App\Traits;

/*
* Trait que incluye methodos comunes para filtrar datos
*/

trait Filterable
{
    public function scopeFilterByNameAndLastname($query, $keyword)
    {
        return $keyword
            ? $query->where(
                fn ($keyword_query) => $keyword_query->where('name', 'LIKE', "%$keyword%")
                ->orWhere('last_name', 'LIKE', "%$keyword%")
            )
            : $query;
    }

    public function scopeFilterByPatientName($query, $patient_name)
    {
        return $patient_name
            ? $query->whereHas('patient', fn ($patient) => $patient->where(
                fn ($patient_query) => $patient_query->where('name', 'LIKE', "%$patient_name%")
                ->orWhere('last_name', 'LIKE', "%$patient_name%")
            ))
            : $query;
    }

    public function scopeFilterExamByNameOrDoctorOrPatient($query, $name)
    {
        return $name
            ? $query->where(function ($query) use ($name) {
                $query->where('name', 'LIKE', "%$name%")
                      ->orWhereHas('appointment', function ($appointment) use ($name) {
                          $appointment->whereHas('doctor', function ($doctor) use ($name) {
                              $doctor->where('name', 'LIKE', "%$name%")
                                     ->orWhere('last_name', 'LIKE', "%$name%");
                          })
                          ->orWhereHas('patient', function ($patient) use ($name) {
                              $patient->where('name', 'LIKE', "%$name%")
                                     ->orWhere('last_name', 'LIKE', "%$name%");
                          });
                      });
            })
            : $query;
    }

    public function scopeFilterPrescriptionByNameOrDoctorOrPatient($query, $name)
    {
        return $name
            ? $query->where(function ($query) use ($name) {
                $query->where('drug', 'LIKE', "%$name%")
                      ->orWhereHas('appointment', function ($appointment) use ($name) {
                          $appointment->whereHas('doctor', function ($doctor) use ($name) {
                              $doctor->where('name', 'LIKE', "%$name%")
                                  ->orWhere('last_name', 'LIKE', "%$name%");
                          })
                          ->orWhereHas('patient', function ($patient) use ($name) {
                              $patient->where('name', 'LIKE', "%$name%")
                                     ->orWhere('last_name', 'LIKE', "%$name%");
                          });
                      });
            })
            : $query;
    }
}
