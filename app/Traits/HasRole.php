<?php
namespace App\Traits;

use App\Models\Role;

/**
 * Trait que permite manejar los roles
 */
trait HasRole
{
    /*
    * Metodo que devuelve la relación rol
    */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /*
    * Metodo que valida si el usuario tiene el rol especificado
    */
    public function hasRole(String $role)
    {
        return $this->role->name === $role;
    }

    /*
    * Metodo que valida si el usuario no tiene el rol especificado
    */
    public function doesntHaveRole(String $role)
    {
        return $this->role->name !== $role;
    }

    public function hasAnyRole(array $roles)
    {
        $roles = collect($roles);
        return $roles->contains($this->role->name);
    }

    /*
    * Metodo que asigna un rol al usuario
    */
    public function assignRole(String $role)
    {
        $role = Role::where('name', $role)->firstOrFail();
        return $this->role()->associate($role);
    }

    /*
    * Metodo que consulta los usuarios que tienen cualquiera de los roles especificados
    */
    public function scopeWhereHasAnyRole($query, array $roles)
    {
        return $query->whereHas('role', fn($role) => $role->whereIn('name', $roles));
    }

    /*
    * Metodo que consulta los usuarios con el rol especificado
    */
    public function scopeWhereHasRole($query, String $role_name)
    {
        return $query->whereHas('role', fn($role) => $role->where('name', $role_name));
    }

    /*
    * Metodo que consulta los usuarios que no tienen el role especificado
    */
    public function scopeWhereDoesntHaveRole($query, string $role_name)
    {
        return $query->whereHas('role', fn($role) => $role->where('roles.name', '<>', $role_name));
    }

    /*
    * Metodo que valida si el usuario es un super usuario
    */
    public function getIsSuperAttribute()
    {
        return $this->hasRole('super');
    }

    /*
    * Metodo que valida si el usuario es un administrador
    */
    public function getIsAdminAttribute()
    {
        return $this->hasRole('admin');
    }

    /*
    * Metodo que valida si el usuario es un doctor
    */
    public function getIsDoctorAttribute()
    {
        return $this->hasRole('doctor');
    }

    /*
    * Metodo que valida si el usuario es un administrador
    */
    public function getIsClientAttribute()
    {
        return $this->hasRole('client');
    }
}
