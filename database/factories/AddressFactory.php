<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Services\ProvinceService;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $province = new ProvinceService();
        $province_name = $this->faker->randomElement($province->getProvincesNames());
        $city_name = $this->faker->randomElement($province->getCitiesNameFromProvinces($province_name));
        return [
            'main_street'       => $this->faker->streetName,
            'secondary_street'  => $this->faker->streetName,
            'province'          => Str::upper($province_name),
            'city'              => Str::upper($city_name)
        ];
    }
}
