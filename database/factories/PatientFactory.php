<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'identification_card'   => $this->faker->unique()->regexify('^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$'),
            'name'                  => $this->faker->name,
            'last_name'             => $this->faker->lastName,
            'email'                 => $this->faker->unique()->safeEmail,
            'phone'                 => $this->faker->regexify('^09[0-9]{8}'),
            'birthdate'             => $this->faker->date(),
            'gender'                => $this->faker->randomElement([ Patient::FEMALE_GENDER, Patient::MALE_GENDER ])
        ];
    }
}
