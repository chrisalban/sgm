<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->timestamp('start_time')->nullable();
            $table->timestamp('end_time')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('is_cancelled')->default(false);
            $table->timestamp('closed_at')->nullable();
            $table->foreignId('patient_id')->constrained()->onUpdate('cascade');
            $table->foreignId('doctor_id')->constrained('users')->onUpdate('cascade');
            $table->foreignId('speciality_id')->constrained()->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
