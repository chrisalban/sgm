<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Speciality;

class SpecialitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Speciality::factory()->count(15)->create(); */
        
        Speciality::create([
            'name' => 'Medicina Interna'
        ]);
        Speciality::create([
            'name' => 'Pediatría'
        ]);
        Speciality::create([
            'name' => 'Endocrinología'
        ]);
        Speciality::create([
            'name' => 'Ginecología'
        ]);
    }
}
