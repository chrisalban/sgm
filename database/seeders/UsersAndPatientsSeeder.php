<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use App\Models\User;
use App\Models\Address;
use App\Models\Patient;
use App\Models\Role;

class UsersAndPatientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();

        /* Super user */
        User::factory()
            ->for(Address::factory()->create())
            ->make([
                'identification_card'   => '1700000001',
                'name'                  => 'Super',
                'last_name'             => 'Super',
                'username'              => 'super',
                'email'                 => 'super@mediquito.com',
                'email_verified_at'     => now(),
                'password'              => bcrypt('Eloriginaloctaviorex@69'), //password
                'phone'                 => '0987654321',
                'birthdate'             => now(),
                'gender'                => Arr::random(['F', 'M']),
            ])
            ->assignRole('super')->save();

        if (app()->environment('local')) {
            /* Admin user */
            User::factory()
                ->for(Address::factory()->create())
                ->make([
                    'identification_card'   => '1700000002',
                    'name'                  => 'Admin',
                    'last_name'             => 'Admin',
                    'username'              => 'admin',
                    'email'                 => 'admin@admin.com',
                    'email_verified_at'     => now(),
                    'password'              => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
                    'phone'                 => '0987654321',
                    'birthdate'             => now(),
                    'gender'                => Arr::random(['F', 'M']),
                ])
                ->assignRole('admin')->save();

            /* Doctor user */
            $doctor = User::factory()
                ->for(Address::factory()->create())
                ->make([
                    'identification_card'   => '1700000003',
                    'name'                  => 'Doctor',
                    'last_name'             => 'Doctor',
                    'username'              => 'doctor',
                    'email'                 => 'doctor@doctor.com',
                    'email_verified_at'     => now(),
                    'password'              => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
                    'phone'                 => '0987654321',
                    'birthdate'             => now(),
                    'gender'                => Arr::random(['F', 'M']),
                ]);
            $doctor->assignRole('doctor')->save();
            $doctor->specialities()->attach([1, 2, 3, 4]);

            /* Hospital users */
            /* User::factory() */
            /*     ->times(19) */
            /*     ->for(Address::factory()->create()) */
            /*     ->make() */
            /*     ->each(function ($user) { */
            /*         $role = Arr::random(['admin', 'doctor']); */
            /*         $user->assignRole($role); */
            /*         $user->save(); */
            /*         if ($role === 'doctor') { */
            /*             $user->specialities()->attach(rand(1, 15)); */
            /*         } */
            /*     }); */

            /* Clientes */
            $relatonships = collect([ Patient::SPOUSE_RELATIONSHIP, Patient::SON_RELATIONSHIP, Patient::DAUGHTER_RELATIONSHIP, Patient::OTHERS_RELATIONSHIP]);

            /* Client user */
            $address = Address::factory()->create();
            User::factory()
                ->for($address)
                ->hasAttached(
                    Patient::factory()->count(4)->for($address),
                    fn () => [ 'relationship' => $relatonships->random() ]
                )
                ->for($roles->firstWhere('name', 'client'))
                ->hasAttached(Patient::factory()
                                    ->for($address)
                                    ->create([
                                        'identification_card'   => '1700000004',
                                        'name'                  => 'Client',
                                        'last_name'             => 'Client',
                                        'email'                 => 'client@client.com',
                                        'phone'                 => '0987654321',
                                        'birthdate'             => now(),
                                        'gender'                => Arr::random(['F', 'M']),
                                    ]), fn () => [ 'relationship' => Patient::HEADLINE_RELATIONSHIP ])
                ->create([
                    'identification_card'   => '1700000004',
                    'name'                  => 'Client',
                    'last_name'             => 'Client',
                    'username'              => 'client',
                    'email'                 => 'client@client.com',
                    'email_verified_at'     => now(),
                    'password'              => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
                ]);

            /* /1* Patients with a Client user *1/ */
            /* User::factory() */
            /*     ->times(100) */
            /*     ->for($roles->firstWhere('name', 'client')) */
            /*     ->make() */
            /*     ->each(function ($user) { */
            /*         $address = Address::factory()->create(); */
            /*         $user->address()->associate($address)->save(); */
            /*         $user->patients()->attach(Patient::factory()->for($address)->create(), [ 'relationship' => Patient::HEADLINE_RELATIONSHIP ]); */
            /*     }); */
        }
    }
}
