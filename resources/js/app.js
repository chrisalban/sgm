require('./bootstrap');
import Vue from 'vue'
import { BootstrapVue } from 'bootstrap-vue'
import FontAwesomeIcon from './icons'
import router from './router'
import store from './store'

import dayjs from 'dayjs'
require('dayjs/locale/es')
dayjs.locale('es')

Vue.component('app', require('./components/App').default)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(BootstrapVue)

new Vue({
    el: '#app',
    router,
    store
})
