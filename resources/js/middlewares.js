import RouterMiddleware from './services/Middleware'
import store from './store'
import Patient from './models/Patient'
import Appointment from './models/Appointment'

let isAuthRoute = to =>
    to.name === 'login' ||
    to.name === 'register' ||
    to.name === 'forgot-password' ||
    to.name === 'reset-password' ||
    to.name === 'verification.notice'

const middleware = new RouterMiddleware([
    {
        name: 'role',
        handler: (to, from, next, params) => {
            if (store.state.user.hasAnyRole(...params)) next()
            else next({ name: '403' })
        }
    },
    {
        name: 'auth',
        handler: (to, from, next, params) => {
            store.dispatch('verifyAuthenticatedUser')
            .then(() => {
                if(!isAuthRoute(to) && to.path !== '/') next()
                else next({ name: 'home' })
            })
            .catch(errors => {
                let redirectTo = {}
                switch (errors.status) {
                    case 401:
                        redirectTo = { name: 'login' }
                        break;
                    case 403:
                        redirectTo = { name: 'verification.notice' }
                        break;
                    default:

                }
                if(!isAuthRoute(to)) next(redirectTo)
                else next()
            })
        }
    },
    {
        name: 'has-acces-to-patient',
        handler: (to, from, next, params) => {
            Patient.findById(to.params.patient)
            .then( () => next())
            .catch( errors => {
                switch (errors.status) {
                    case 403:
                        next({ name: '403' })
                        break
                    default:
                        next({ name: '500' })
                }
            })
        }
    },
    {
        name: 'has-acces-to-appointment',
        handler: (to, from, next, params) => {
            Appointment.findById(to.params.appointment)
            .then( () => next())
            .catch( errors => {
                switch (errors.status) {
                    case 403:
                        next({ name: '403' })
                        break
                    default:
                        next({ name: '500' })
                }
            })
        }
    }
])

export default middleware
