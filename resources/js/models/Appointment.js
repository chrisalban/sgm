/*
* Clase que permite la conección con la api de datos de los usuarios.
*/
import Structure from '../services/Orm'
import Patient from './Patient'
import Prescription from './Prescription'
import Speciality from './Speciality'
import User from './User'
import Exam from './Exam'

export default new Structure.Factory({
    attribute: 'appointments',
    fillable: [
        'startTime',
        'endTime',
        'description',
        'isCancelled',
        'closedAt'
    ],
    relationships: [
        {
            attribute: 'patient',
            type: 'hasOne',
            model: Patient
        },
        {
            attribute: 'doctor',
            type: 'hasOne',
            model: User
        },
        {
            attribute: 'speciality',
            type: 'hasOne',
            model: Speciality
        },
        {
            attribute: 'prescriptions',
            type: 'hasMany',
            model: Prescription
        },
        {
            attribute: 'exams',
            type: 'hasMany',
            model: Exam
        },
    ],
    methods: {
        reschedule(params) {
            return new Promise( (resolve, reject) => {
                let fillableParams = {...this.fillableParams, ...params}
                axios.put(`/api/appointments/reschedule/${this.id}`, fillableParams)
                .then( response => resolve(response))
                .catch( errors => reject(errors.response))
            })
        },
        updateDescription() {
            return new Promise( (resolve, reject) => {
                axios.put(`/api/appointments/description/${this.id}`, { description: this.description })
                .then( response => resolve(response) )
                .catch( errors => reject(errors.response) )
            })
        },
        close() {
            return new Promise( (resolve, reject) => {
                axios.put(`/api/appointments/close/${this.id}`)
                .then( response => resolve(response) )
                .catch(errors => reject(errors.response))
            })
        },
        downloadPrescription() {
            return new Promise((resolve, reject) => {
                axios({
                    url: `/api/appointments/prescriptions/pdf/${this.id}`,
                    method: 'get',
                    responseType: 'blob'
                })
                .then( response => resolve(response))
                .catch( errors => reject(errors.response))
            })
        },
    }
})
