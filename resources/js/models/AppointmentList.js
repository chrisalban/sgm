/*
* Clase que permite la conección con la api de datos de los usuarios.
*/
import Structure from '../services/Orm'
import Patient from './Patient'
import Prescription from './Prescription'
import Speciality from './Speciality'
import User from './User'

export default new Structure.Factory({
    attribute: 'appointments',
    fillable: [
        'startTime',
        'endTime',
        'description',
        'isCancelled'
    ],
    relationships: [
        {
            attribute: 'patient',
            type: 'hasOne',
            model: Patient
        },
        {
            attribute: 'doctor',
            type: 'hasOne',
            model: User
        },
        {
            attribute: 'speciality',
            type: 'hasOne',
            model: Speciality
        },
        {
            attribute: 'prescriptions',
            type: 'hasMany',
            model: Prescription
        },
    ],
    methods: {
        reschedule(params) {
            return new Promise( (resolve, reject) => {
                let fillableParams = {...this.fillableParams, ...params}
                console.log(fillableParams)
                axios.put(`/api/appointments/reschedule/${this.id}`, fillableParams)
                .then( response => resolve(response))
                .catch( errors => reject(errors.response))
            })
        },
        updateDescription() {
            return new Promise( (resolve, reject) => {
                axios.put(`/api/appointments/description/${this.id}`, { description: this.description })
                .then( response => resolve(response) )
                .catch( errors => reject(errors.response) )
            })
        }
    }
})
