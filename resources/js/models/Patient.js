/*
* Clase que permite la conección con la api de datos de los usuarios.
*/
import Structure from '../services/Orm'
import Address from './Address'

export default new Structure.Factory({
    attribute: 'patient',
    fillable: [
        'identificationCard',
        'name',
        'lastName',
        'email',
        'phone',
        'birthdate',
        'gender',
        'relationship'
    ],
    computed: {
        fullName() {
            return `${this.name} ${this.lastName}`
        },
        relationshipName() {
            let relationship
            switch (this.relationship) {
                case 'HEADLINE':
                    relationship = 'Titular'
                    break;
                case 'SPOUSE':
                    relationship = 'Esposo'
                    break;
                case 'SON':
                    relationship = 'Hijo'
                    break;
                case 'DAUGHTER':
                    relationship = 'Hija'
                    break;
                case 'OTHERS':
                    relationship = 'Otros'
                    break;
                default:
                    relationship = null
            }
            return relationship
        }
    },
    relationships: [
        {
            attribute: 'address',
            type: 'hasOne',
            model: Address
        }
    ]
})
