/*
* Clase que permite la conección con la api de datos de los usuarios.
*/
import Structure from '../services/Orm'
import Appointment from './Appointment'
import Patient from './Patient'
import User from './User'

export default new Structure.Factory({
    attribute: 'prescriptions',
    fillable: [
        'drug',
        'medication',
    ],
    relationships: [
        {
            attribute: 'appointment',
            type: 'hasOne',
            model: Appointment
        },
        {
            attribute: 'doctor',
            type: 'hasOne',
            model: User
        },
        {
            attribute: 'patient',
            type: 'hasOne',
            model: Patient
        }
    ],
})
