/*
* Vuex configuration
*/
import Vue from 'vue'
import Vuex from 'vuex'
import User from './models/User'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        user: null,
        title: '',
        searchKeyword: '',
        searchPlaceholder: '',
        showSearch: false,
        isShowingAlert: false,
        alertMessage: '',
        alertVariant: 'success',
        date: ''
    },
    mutations: {
        setUser(state, user) {
            state.user = User.make(user)
        },
        setTitle(state, title) {
            state.title = title
        },
        setSearchKeyword(state, keyword) {
            state.searchKeyword = keyword
        },
        setSearchPlaceholder(state, placeholder) {
            state.searchPlaceholder = placeholder
        },
        setShowSearch(state, show) {
            state.showSearch = show
        },
        setVisibilityAlert(state, isShowingAlert) {
            state.isShowingAlert = isShowingAlert
        },
        setAlertMessage(state, message) {
            state.alertMessage = message
        },
        setAlertVariant(state, variant) {
            state.alertVariant = variant
        },
        setDateFilter(state, date) {
            state.date = date
        }
    },
    actions: {
        verifyAuthenticatedUser(context) {
            return new Promise((resolve, reject) => {
                axios.get('/api/user')
                .then( response => {
                    context.commit('setUser', response.data.data)
                    resolve(response)
                })
                .catch( error => {
                    context.commit('setUser', null)
                    reject(error.response)
                })
            })
        },
        showSearchBar(context) {
            context.commit('setShowSearch', true)
        },
        hideSearchBar(context) {
            context.commit('setShowSearch', false)
        },
        showSuccesAlert(context) {
            context.commit('setVisibilityAlert', true)
            context.commit('setAlertVariant', 'success')
        },
        showDangerAlert(context) {
            context.commit('setVisibilityAlert', true)
            context.commit('setAlertVariant', 'danger')
        },
        hideAlert(context) {
            context.commit('setVisibilityAlert', false)
            context.commit('setAlertMessage', '')
        },
    }
})

export default store
