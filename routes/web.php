<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('guest')->group( function() {

    Route::get('/{any}', fn() => view('index'))->where(['any' => 'login|forgot-password|register']);

    Route::post('/login', [ LoginController::class, 'authenticate' ])->name('login');

    Route::post('/forgot-password', [ ResetPasswordController::class, 'sendEmailForm' ])->name('password.email');

    Route::get('/reset-password/{token}', fn() => view('index'))->name('password.reset');

    Route::post('/reset-password', [ ResetPasswordController::class, 'resetPassword' ]);

    Route::post('/register', [ RegisterController::class, 'register' ]);

});

Route::middleware('auth')->group(function() {
    Route::get('/email/verify', fn() => view('index'))->name('verification.notice');

    Route::middleware('signed')
        ->get('/email/verify/{id}/{hash}', [ LoginController::class, 'verifyEmail' ])
        ->name('verification.verify');

    Route::middleware('throttle:6,1')
        ->post('/email/verification-notification', [ LoginController::class, 'resendVerificationEmail' ])
        ->name('verification.send');

    Route::post('/logout', [ LoginController::class, 'deauthenticate' ])->name('logout');
});

Route::fallback(fn() => view('index'))->middleware([ 'verified', 'auth:sanctum' ]);
