<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login()
    {
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory())
                    ->create();

        $response = $this->post('/login', [
            'username' => $user->username,
            'password' => 'password',
            'remember' => false
        ]);

        $response->assertRedirect('/');
    }

    public function test_logout()
    {
         $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory())
                    ->create();

         Sanctum::actingAs($user);

         $this->post('/logout');

         $this->assertGuest('api');
    }
}
