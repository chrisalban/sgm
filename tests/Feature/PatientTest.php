<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\Patient;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PatientTest extends TestCase
{
   use RefreshDatabase, WithFaker;

    public function test_list_patients()
    {
        $user = User::factory()
                    ->for(Address::factory()
                                ->create()
                         )
                    ->for(Role::factory()
                              ->create([
                                    'name' => 'client'
                              ])
                    )
                    ->create();

        $patients = Patient::factory()
                           ->for(Address::factory())
                           ->hasAttached($user , [ 'relationship' => Patient::HEADLINE_RELATIONSHIP ])
                           ->create();


        $response = $this->get('/api/patients');

        $response->assertRedirect('/login');

        $user = $patients->users->first();
        Sanctum::actingAs($user);
        
        $response = $this->get('/api/patients');

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                [
                    'id' => $patients->id,
                    'name' => $patients->name,
                    'last_name' => $patients->last_name,
                    'relationship' => $patients->users->first()->pivot->relationship,
                ]
            ]
        ]);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->get('/api/patients');

        $response->assertStatus(200);

        $user->role->update([ 'name' => 'admin' ]);

        $response = $this->get('/api/users');

        $response->assertStatus(200);
    }

    public function test_create_patients()
    {
        $data = collect([
            'identification_card'   => $this->faker->regexify('^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$'),
            'name'                  => $this->faker->name,
            'last_name'             => $this->faker->lastName,
            'email'                 => $this->faker->safeEmail,
            'phone'                 => $this->faker->regexify('^09[0-9]{8}'),
            'birthdate'             => $this->faker->date(),
            'gender'                => $this->faker->randomElement([ User::FEMALE_GENDER, User::MALE_GENDER ]),
            'relationship'          => $this->faker->randomElement([ Patient::DAUGHTER_RELATIONSHIP, Patient::SON_RELATIONSHIP, Patient::SPOUSE_RELATIONSHIP, Patient::OTHERS_RELATIONSHIP ]),
            'address'               => [
                'main_street'       => $this->faker->word,
                'secondary_street'  => $this->faker->word,
                'city'              => $this->faker->word,
                'province'          => $this->faker->word,
            ]
        ]);

        $db = $data->only([
            'identification_card',
            'name',
            'last_name',
            'username',
            'email',
            'phone',
            'birthdate',
            'gender',
        ])->all();

        $response = $this->post('/api/patients', $data->all());

        $response->assertRedirect('/login');

        $this->assertDatabaseMissing('patients', $db);

        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->create();

        Sanctum::actingAs($user);

        $response = $this->post('/api/patients', $data->all());

        $response->assertStatus(200);

        $this->assertDatabaseHas('patients', $db);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->post('/api/patients', $data->all());

        $response->assertForbidden();

        $user->role->update([ 'name' => 'client' ]);

        Patient::firstWhere('identification_card', $data['identification_card'])->delete();
        $response = $this->post('/api/patients', $data->all());

        $response->assertStatus(200);
    }

    public function test_update_patients()
    {
        $user = User::factory()
                    ->for(Address::factory()
                                ->create()
                         )
                    ->for(Role::factory()
                              ->create([
                                    'name' => 'client'
                              ])
                    )
                    ->create();

        $update_patient = Patient::factory()
                           ->for(Address::factory())
                           ->hasAttached($user , [ 'relationship' => Patient::HEADLINE_RELATIONSHIP ])
                           ->create();
        
        $data = collect([
            'identification_card'   => $this->faker->regexify('^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$'),
            'name'                  => $this->faker->name,
            'last_name'             => $this->faker->lastName,
            'email'                 => $this->faker->safeEmail,
            'phone'                 => $this->faker->regexify('^09[0-9]{8}'),
            'birthdate'             => $this->faker->date(),
            'gender'                => $this->faker->randomElement([ User::FEMALE_GENDER, User::MALE_GENDER ]),
            'relationship'          => $this->faker->randomElement([ Patient::DAUGHTER_RELATIONSHIP, Patient::SON_RELATIONSHIP, Patient::SPOUSE_RELATIONSHIP, Patient::OTHERS_RELATIONSHIP ]),
            'address'               => [
                'main_street'       => $this->faker->word,
                'secondary_street'  => $this->faker->word,
                'city'              => $this->faker->word,
                'province'          => $this->faker->word,
            ]
        ]);

        $db = $data->only([
            'identification_card',
            'name',
            'last_name',
            'username',
            'email',
            'phone',
            'birthdate',
            'gender',
        ])->all();

        $response = $this->put("/api/patients/$update_patient->id", $data->all());

        $response->assertRedirect('/login');

        $this->assertDatabaseMissing('patients', $db);

        Sanctum::actingAs($user);

        $response = $this->put("/api/patients/$update_patient->id", $data->all());

        $response->assertStatus(200);

        $this->assertDatabaseHas('patients', $db);

        $data = collect([
            'identification_card'   => $this->faker->regexify('^[0-2]{1}[1-9]{1}[0-5]{1}[0-9]{7}$'),
            'name'                  => $this->faker->name,
            'last_name'             => $this->faker->lastName,
            'email'                 => $this->faker->safeEmail,
            'phone'                 => $this->faker->regexify('^09[0-9]{8}'),
            'birthdate'             => $this->faker->date(),
            'gender'                => $this->faker->randomElement([ User::FEMALE_GENDER, User::MALE_GENDER ]),
            'relationship'          => $this->faker->randomElement([ Patient::DAUGHTER_RELATIONSHIP, Patient::SON_RELATIONSHIP, Patient::SPOUSE_RELATIONSHIP, Patient::OTHERS_RELATIONSHIP ]),
            'address'               => [
                'main_street'       => $this->faker->word,
                'secondary_street'  => $this->faker->word,
                'city'              => $this->faker->word,
                'province'          => $this->faker->word,
            ]
        ]);

        $user->role->update([ 'name' => 'admin' ]);

        $update_patient->users()->detach();

        $response = $this->put("/api/patients/$update_patient->id", $data->all());

        $response->assertStatus(200);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->put("/api/patients/$update_patient->id", $data->all());

        $response->assertForbidden();
    }

    public function test_delete_patients()
    {
        $user = User::factory()
                    ->for(Address::factory()
                                ->create()
                         )
                    ->for(Role::factory()
                              ->create([
                                    'name' => 'client'
                              ])
                    )
                    ->create();

        $delete_patient = Patient::factory()
                           ->for(Address::factory())
                           ->hasAttached($user , [ 'relationship' => Patient::OTHERS_RELATIONSHIP ])
                           ->create();

        $response = $this->delete("/api/patients/$delete_patient->id");

        $response->assertRedirect('/login');

        $this->assertDatabaseHas('patients', [
            'id' => $delete_patient->id
        ]);
        
        Sanctum::actingAs($user);

        $response = $this->delete("/api/patients/$delete_patient->id");

        $response->assertStatus(200);

        $this->assertDatabaseHas('patients', [
            'id' => $delete_patient->id
        ]);

        $user->role->update([ 'name' => 'doctor' ]);
        $delete_patient->users()->detach();

        $response = $this->delete("/api/patients/$delete_patient->id");

        $response->assertForbidden();

        $delete_patient = Patient::factory()
                    ->for(Address::factory())
                    ->create();

        $user->role->update([ 'name' => 'admin' ]);

        $response = $this->delete("/api/patients/$delete_patient->id");

        $response->assertStatus(200);
    }
}
