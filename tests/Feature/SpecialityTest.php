<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\Role;
use App\Models\Speciality;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class SpecialityTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_list_specialities()
    {
        $response = $this->get('/api/specialities');

        $response->assertRedirect('/login');
        
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->create();

        $speciality = Speciality::factory()->create();

        Sanctum::actingAs($user);
        
        $response = $this->get('/api/specialities');

        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                [
                    'name' => $speciality->name
                ]
            ]
        ]);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->get('/api/specialities');

        $response->assertStatus(200);

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->get('/api/specialities');

        $response->assertStatus(200);
    }

    public function test_create_specialities()
    {
        $speciality = Speciality::factory()->make();

        $response = $this->post('/api/specialities', [
            'name' => $speciality->name
        ]);

        $response->assertRedirect('/login');

        $this->assertDatabaseMissing('specialities', [
            'name' => $speciality->name
        ]);
        
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->create();

        Sanctum::actingAs($user);
        
        $response = $this->post('/api/specialities', [
            'name' => $speciality->name
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('specialities', [
            'name' => $speciality->name
        ]);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->post('/api/specialities', [
            'name' => $speciality->name
        ]);

        $response->assertForbidden();

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->post('/api/specialities', [
            'name' => $speciality->name
        ]);

        $response->assertForbidden();
    }

    public function test_update_specialities()
    {
        $speciality = Speciality::factory()->create();

        $new_name = $this->faker->word;

        $response = $this->put("/api/specialities/$speciality->id", [
            'name' => $new_name
        ]);

        $response->assertRedirect('/login');

        $this->assertDatabaseMissing('specialities', [
            'name' => $new_name
        ]);
        
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory()->create([ 'name' => 'admin' ]))
                    ->create();

        Sanctum::actingAs($user);
        
        $response = $this->put("/api/specialities/$speciality->id", [
            'name' => $new_name
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('specialities', [
            'name' => $new_name
        ]);

        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->put("/api/specialities/$speciality->id", [
            'name' => $new_name
        ]);

        $response->assertForbidden();

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->put("/api/specialities/$speciality->id", [
            'name' => $new_name
        ]);

        $response->assertForbidden();
    }

    public function test_delete_specialities()
    {
        $speciality = Speciality::factory()->create();

        $response = $this->delete("/api/specialities/$speciality->id");

        $response->assertRedirect('/login');

        $this->assertDatabaseHas('specialities', [
            'name' => $speciality->name
        ]);
        
        $user = User::factory()
                    ->for(Address::factory())
                    ->for(Role::factory())
                    ->create();

        Sanctum::actingAs($user);
        
        $user->role->update([ 'name' => 'doctor' ]);

        $response = $this->delete("/api/specialities/$speciality->id");

        $response->assertForbidden();

        $user->role->update([ 'name' => 'client' ]);

        $response = $this->delete("/api/specialities/$speciality->id");

        $response->assertForbidden();

        $user->role->update([ 'name' => 'admin' ]);

        $response = $this->delete("/api/specialities/$speciality->id");

        $response->assertStatus(200);

        $this->assertDatabaseMissing('specialities', [
            'name' => $speciality->name
        ]);
    }
}
